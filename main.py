#!/usr/bin/env python

from numpy import empty
from model import GanttModel
import matplotlib.pyplot as plt
import visualisations
import time

visualisations.start_server({
    "N": 10,
    "M": 20,
    "visualisations": [
        visualisations.GanttChart(),
        visualisations.TaskDependencies(),
    ]
})

empty_model = GanttModel(10, 20)

# visualisations.taskDependencies(empty_model)

#visualisations.ganttChart(empty_model.tasks, 130)

# viz = visualisations.TaskDependencies()
# viz2 = visualisations.GanttChart()

# viz.update(empty_model)

# while True:
#     # Pause without stealing focus
#     plt.gcf().canvas.draw_idle()
#     plt.gcf().canvas.start_event_loop(0.04)

#     empty_model.step()
#     # visualisations.taskDependencies(empty_model)
#     viz2.update(empty_model)