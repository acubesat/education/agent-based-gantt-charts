
import matplotlib.pyplot as plt
import networkx as nx
import model

from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer

plt.ion()

def start_server(args):
    grid = CanvasGrid(agent_portrayal, 10, 10, 900, 900)
    server = ModularServer(model.GanttModel, [grid], "Gantt Model", args)
    server.port = 8251

    server.launch()

def colour_status(status):
    if status == status.BLOCKED:
        colour = '#bf616a'
    elif status == status.WAITING:
        colour = '#5e81ac'
    elif status == status.IN_PROGRESS:
        colour = '#b48ead'
    elif status == status.DONE:
        colour = '#a3be8c'

    return colour

def agent_portrayal(agent):
    if isinstance(agent, model.Task):
        task = agent

        colour = "gray"
        text = "???"
        filled = "false"

        if task.status == task.status.BLOCKED:
            colour = '#bf616a'
            text = "⛔"
        elif task.status == task.status.WAITING:
            colour = '#5e81ac'
            text = ""
        elif task.status == task.status.IN_PROGRESS:
            colour = '#b48ead'
            text = "…"
        elif task.status == task.status.DONE:
            colour = '#a3be8c'
            text = "✓"
            filled = "false"


        portrayal = {"Shape": "rect",
                    "Color": colour,
                    "Filled": filled,
                    "Layer": 0,
                    "text": task.id,
                    "text_color": "white",
                    "w": 0.8,
                    "h": 0.8}
        return portrayal
    else:
        if agent.currentTask == None:
            color = "gray"
        else:
            color = "red"

        portrayal = {"Shape": "circle",
                    "Color": color,
                    "Filled": "true",
                    "Layer": 0,
                    "text": agent.name,
                    "text_color": "white",
                    "r": min(1, agent.motivation / 100)}
        return portrayal

class GanttChart():
    def __init__(self):
        self.fig = plt.figure()

    def update(self, gmodel):
        self.fig.clf()

        ax = self.fig.subplots(1)

        for task in gmodel.tasks:
            if task.started_time != None:
                for prerequisite in task.prerequisites:
                    x1 = prerequisite.started_time + prerequisite.actual_duration
                    x2 = task.started_time - 0.2
                    if prerequisite.assignee is None:
                        y1 = -1
                    else:
                        y1 = prerequisite.assignee.unique_id
                    if task.assignee is None:
                        y2 = -1
                    else:
                        y2 = task.assignee.unique_id

                    ax.arrow(x1, y1, x2 - x1, y2 - y1, head_width=0.2, alpha=0.3)

        for idx, task in enumerate(gmodel.tasks):
            if task.started_time != None:
                if task.status == model.Status.DONE:
                    duration = task.actual_duration
                else:
                    duration = gmodel.current_step - task.started_time

                if task.assignee is None:
                    y = -1
                else:
                    y = task.assignee.unique_id

                ax.barh(y, duration, left=task.started_time, color=colour_status(task.status))
                ax.text(task.started_time - 0.1, y, task.id, va='center', ha='right', alpha=0.8)

        # Remove Y axis ticks
        ax.set_yticks(range(gmodel.num_agents))
        ax.set_yticklabels(map(lambda a: a.name, gmodel.schedule.agent_buffer()))
        # ax.set_yticks([0,1,2], labels=["a","b","c"])

        # Grid lines
        ax.set_axisbelow(True)
        ax.xaxis.grid(color='gray', linestyle='dashed', alpha=0.2, which='both')

        # plt.show()

# def ganttChart(tasks, duration):


class TaskDependencies():
    def __init__(self):
        self.fig = plt.figure()

    def update(self, model):
        if model.current_step % 20 != 0:
            return

        self.fig.clf()

        # List of node colours for visualisation
        colours = []
        for task in model.tasks:
            if task.status == task.status.BLOCKED:
                colours.append('#bf616a')
            elif task.status == task.status.WAITING:
                colours.append('#5e81ac')
            elif task.status == task.status.IN_PROGRESS:
                colours.append('#b48ead')
            elif task.status == task.status.DONE:
                colours.append('#a3be8c')

        ax = self.fig.subplots(1)
        plt.figure(self.fig.number)
        nx.draw_networkx(model.get_graph(), ax=ax,
            pos=nx.planar_layout(model.get_graph()),
            nodelist=range(len(model.tasks)), node_color=colours
        )
