from mesa import Agent, Model
from mesa.time import RandomActivation
from enum import Enum
from pprint import pprint
import networkx as nx
from more_itertools import quantify
from mesa.space import MultiGrid

import matplotlib.pyplot as plt

from faker import Faker
fake = Faker()
Faker.seed(4321)

class Status(Enum):
    BLOCKED = 1,
    WAITING = 2,
    IN_PROGRESS = 3,
    DONE = 4

class Task():
    """A schedulable task"""
    def __init__(self, id, name):
        self.name = name
        self.id = id
        self.prerequisites = []
        self.assignee = None

        self.estimated_duration = 0
        self.actual_duration = 0
        self.completes_self = False

        self.scheduled_time = 0
        self.started_time = None

        self.status = Status.WAITING
        self.progress = 0

        self.visited = False

    def randomise(self, fake):
        #TODO: Use better distributions for this??
        self.estimated_duration = fake.random_int(0, 30)
        self.actual_duration = max(0, self.estimated_duration + fake.random_int(-5, 20))
        self.scheduled_time = fake.random_int(0, 100)

    def propagate_status(self):
        if self.visited == True:
            return

        if self.progress >= 1:
            self.status = Status.DONE
        elif self.progress > 0:
            self.status = Status.IN_PROGRESS
        else:
            for prerequisite in self.prerequisites:
                prerequisite.propagate_status()

            incomplete_prerequisites = quantify(self.prerequisites, lambda t: t.status != Status.DONE)

            if incomplete_prerequisites <= 0:
                self.status = Status.WAITING
            else:
                self.status = Status.BLOCKED

        self.visited = True

    def step():
        # Investigate prerequisite status
        pass

class PersonAgent(Agent):
    GLOBAL_MOTIVATION = 0.05
    # GLOBAL_MOTIVATION = 1000
    MOTIVATION_INCREASE = 0.5
    TIREDNESS_INCREASE = 1
    TIREDNESS_DECREASE = 1

    """A person who can complete tasks."""
    def __init__(self, unique_id, model, name):
        super().__init__(unique_id, model)
        self.name = name
        self.tiredness = 50
        self.motivation = 50
        self.tasks = []

        self.currentTask = None

        print(self.name)

    def add_task(self, task):
        self.tasks.append(task)

    def start_a_task(self):
        for task in self.tasks:
            if task.status == Status.WAITING:
                self.currentTask = task
                task.started_time = self.model.current_step
                break

    def perform_current_task(self):
        task = self.currentTask
        task.progress += 1 / (task.actual_duration + 1)

        if task.progress >= 1:
            task.progress = 1
            task.status = Status.DONE
            self.currentTask = None
            self.motivation = 0

    def step(self):
        current_motivation = self.motivation * self.GLOBAL_MOTIVATION

        if self.currentTask == None:
            # Am I motivated enough to start a new task?
            if fake.boolean(current_motivation):
                # Motivation found. Start a new task
                self.start_a_task()
            else:
                pass
                # print("not enough motivation [" + str(current_motivation) + "]")

        if self.currentTask is None:
            self.motivation += self.MOTIVATION_INCREASE
            self.tiredness -= self.TIREDNESS_DECREASE
        else:
            self.perform_current_task()
            self.tiredness += self.TIREDNESS_INCREASE


class GanttModel(Model):
    """A model with some number of agents."""
    def __init__(self, N, M, visualisations=[]):
        self.num_agents = N
        self.num_tasks = M
        self.schedule = RandomActivation(self)
        self.tasks = []
        self.grid = MultiGrid(N, M, True)
        self.running = True
        self.current_step = 0
        self.graph = None
        self.visualisations = visualisations

        # Create agents
        for i in range(self.num_agents):
            a = PersonAgent(i, self, fake.first_name())

            # Create tasks
            M = fake.random_int(1,4)
            for j in range(M):
                task = Task(len(self.tasks), fake.sentence())
                task.assignee = a
                a.add_task(task)
                self.tasks.append(task)
                self.grid.place_agent(task, (N - 1 - i, 8 - j))

            self.schedule.add(a)
            self.grid.place_agent(a, (N - 1 - i, 9))

        # Add a couple of tasks with no assignee
        for i in range(fake.random_int(1, 5)):
            task = Task(len(self.tasks), fake.company() + " " + fake.bs())
            task.completes_self = True

        for task in self.tasks:
            # Add dependencies for each task
            dependencyCount = fake.random_int(min = 0, max = min(2, len(self.tasks)))
            print(dependencyCount)
            task.prerequisites = fake.random_sample(self.tasks, length = dependencyCount)

            # Generate rest of the required random data
            task.randomise(fake)

        self.remove_cycles()
        self.propagate_task_statuses()
        self.update_visualisations()

        for task in self.tasks:
            pprint(vars(task))

    def get_graph(self, refresh=False):
        if refresh or self.graph == None:
            DG = nx.DiGraph()
            for task in self.tasks:
                DG.add_node(task.id)
            for task in self.tasks:
                for pretask in task.prerequisites:
                    DG.add_edge(task.id, pretask.id)
            self.graph = DG
        return self.graph

    def remove_cycles(self):
        DG = self.get_graph(refresh=True)

        # Remove dual prerequisites
        for task in self.tasks:
            task.prerequisites = list(set(task.prerequisites))

        for cycle in nx.simple_cycles(DG):
            if len(cycle) == 1:
                # An element has itself as a prerequisite
                id = cycle[0]
                task = self.tasks[id]
                task.prerequisites = [x for x in task.prerequisites if x.id != id]
            else:
                parent = cycle[-2]
                child = cycle[-1]
                task = self.tasks[parent]
                task.prerequisites = [x for x in task.prerequisites if x.id != child]

        self.get_graph(refresh=True)

    def propagate_task_statuses(self):
        for task in self.tasks:
            task.visited = False
        for task in self.tasks:
            task.propagate_status()

    def update_visualisations(self):
        for visualisation in self.visualisations:
            visualisation.update(self)
        if len(self.visualisations) > 0:
            plt.gcf().canvas.draw_idle()
            plt.gcf().canvas.start_event_loop(0.01)

    def step(self):
        '''Advance the model by one step.'''
        print("Model step {}".format(self.current_step))

        self.schedule.step()

        self.propagate_task_statuses()

        self.update_visualisations()

        self.current_step += 1

